#!/usr/bin/env python
# coding: utf-8

# # String Literals

# In[4]:


print("Hello")
print('Hello')


# # Assign String to a Variable

# In[5]:


a = "Hello"
print(a)


# # MULTILINE

# In[3]:


a = """LIFE IS LOVE,
don't forget to live,
always smile,
LOVE YOURSELF FIRST"""
print(a)


#  USE three double quotes OR three single quotes

# # Strings are Arrays

# In[6]:


a = "Hello, World!"
print(a[1])


# # Slicing

# In[7]:


b = "Hello, World!"
print(b[2:5])


# # Negative Indexing

# In[8]:


b = "Hello, World!"
print(b[-5:-2])


# # String Length

# In[9]:


a = "Hello, World!"
print(len(a))


# In[10]:


len(a)


# # String Methods

# In[16]:


a = "Hello, World!"
z = print(a.lower())
y = print(a.upper())


# In[17]:


a = "Hello, World!"
print(a.replace("H", "J"))


# In[18]:


a = "Hello, World!"
print(a.split(",")) 


# In[19]:


txt = "The rain in Spain stays mainly in the plain"
x = "ain" in txt
print(x)


# # CONCATENATION

# In[20]:


a = "Hello"
b = "World"
c = a + b
print(c)


# In[22]:


age = 36
txt = "My name is John, and I am {}"
print(txt.format(age))


# In[23]:


quantity = 3
itemno = 567
price = 49.95
myorder = "I want {} pieces of item {} for {} dollars."
print(myorder.format(quantity, itemno, price))


# # Escape Character-An escape character is a backslash \ followed by the character you want to insert.

# In[28]:


txt = 'NAVEEN\'S "LAPTOP"'
print(txt)


# In[ ]:




